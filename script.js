//Header fixed
window.onscroll = () => {
    const header = document.querySelector('.header');
    const scrollY = window.scrollY;

    if (scrollY > 53) {
        header.classList.add('header-fixed');
    } else if (scrollY < 53) {
        header.classList.remove('header-fixed');
    }
}


//Hamburg menu
const hamburgMenu = document.querySelector('.hamburg-menu');
const headerMenu = document.querySelector('.header-menu');
let menuOpen = false;
hamburgMenu.addEventListener('click', () => {
    if (!menuOpen == true) {
        hamburgMenu.classList.add('open');
        menuOpen = true;
        headerMenu.classList.add('active');
    } else {
        hamburgMenu.classList.remove('open');
        menuOpen = false;
        headerMenu.classList.remove('active');
    }
});


//Scroll up
const offset = 100;
const scrollUp = document.querySelector('.scroll-up');
const scrollUpSvgPath = document.querySelector('.scroll-up__svg-path');
const pathLength = scrollUpSvgPath.getTotalLength();
const getTop = () => window.pageYOffset || document.documentElement.scrollTop;

scrollUpSvgPath.style.strokeDasharray = `${pathLength} ${pathLength}`;
scrollUpSvgPath.style.transition = 'stroke-dashoffset 20ms';

const updateDashofset = () => {
    const hieght = document.documentElement.scrollHeight - window.innerHeight;
    const dashOffset = pathLength - (getTop() * pathLength /  hieght);

    scrollUpSvgPath.style.strokeDashoffset = dashOffset;
};

window.addEventListener('scroll', () => {
    updateDashofset();

    if (getTop() > offset) {
        scrollUp.classList.add('scroll-up-active');
    } else {
        scrollUp.classList.remove('scroll-up-active');
    }
});

scrollUp.addEventListener('click', () => {
    window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });
});
